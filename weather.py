# ! python3
import csv
import urllib.request
import json

# Weather object for external tank conditions
class Weather:

	def __init__(self):


		APIKey = "0fa544b8da1bed89852b54e06eb25c37"
		LatLon = "-34.5003,139.0506" # Angaston
		weather_json = urllib.request.urlopen("https://api.darksky.net/forecast/" + APIKey + "/" + LatLon + "?units=si").read()
		weather_json_decoded = json.loads(weather_json)

		self.hour_forecast = []
		for hour in weather_json_decoded['hourly']['data']:
			self.hour_forecast.append(hour['temperature'])


		print(self.hour_forecast)
		self.day_forcast = self.load_file('forecast_temps.csv')

		print('day_forcast',self.day_forcast)
		self.weighting_coefficients = self.load_file('weighting_coefficients.csv')
		print('weighting_coefficients',self.weighting_coefficients)
		month_historical = self.load_file('weather_historical.csv')[1:]
		self.ambient_temp = [float(x[0]) for x in month_historical]
		print('ambient_temp',self.ambient_temp)
		self.convection = [float(x[1]) for x in month_historical]
		print('convection',self.convection)

		self.solar_exposure = [float(x[2]) for x in month_historical]
		print('solar_exposure',self.solar_exposure)

		self.wind_speed = [float(x[3]) for x in month_historical]
		print('wind_speed',self.wind_speed)


	# Load CSV files
	def load_file(self, file_name):
		with open(file_name, 'rt') as csvfile:
			reader = csv.reader(csvfile, delimiter=',')
			lst = []
			for row in reader:
				lst.append(row)
			return lst

	def get_day_temp(self,days_elapsed,month_index):

		coefficient = float(self.weighting_coefficients[month_index][0])
		min = float(self.day_forcast[days_elapsed][0])
		max = float(self.day_forcast[days_elapsed][1])
		ambient_temp = min + coefficient*(max-min)
		
		return ambient_temp

	# Weighted  temperatures based on dau ambient and weighting coeffs
	def set_weighted_day_temps(self,current_time):
		self.weighted_day_temps = []
		for i, day in enumerate(self.day_forcast):
			print(day)
			self.weighted_day_temps.append(float(day[0]) + float(self.weighting_coefficients[i][0])*(float(day[1]) - float(day[0])))
		
	#Solar Radiation
	def get_solar_heat_input(self,month,tank_geometry):
		alpha = 0.27 #solar absorptivity of external coating

		Q_solar_3 = self.solar_exposure[month-1] * alpha * 0.5 * tank_geometry['A3'] #soalar heat input - outside tank wall [W]
		Q_solar_10 = self.solar_exposure[month-1] * alpha * tank_geometry['A10'] #solar heat input - tank top insulated [W]
		Q_solar_12 = self.solar_exposure[month-1] * alpha * tank_geometry['A12'] #soalar heat input - lid un-insulated [W]

		return Q_solar_3, Q_solar_10, Q_solar_12

	# Get ambient temperatures
	def get_ambient_temp(self,current_time,start_time):

		days_available = len(self.day_forcast)
		days_elapsed = current_time.day - start_time.day

		# If the days elapsed exceeds the amount of forcasted days, use monthly data
		if days_available <= days_elapsed:
			ambient_temp = self.ambient_temp[current_time.month]
		else:
			ambient_temp = self.get_day_temp(days_elapsed,current_time.month)

		return ambient_temp
