#! python3
import datetime
import csv
import math
import time

from weather import Weather
from tank import Tank

weather = Weather()
tank = Tank(weather)

temp_initial = 18.0 # deg
temp_setpoint = 9.0 # deg
increment = 1 #minutes

t_start = datetime.datetime(2017, 1, 1, 14, 00, 00)
t_finish = datetime.datetime(2017, 1, 10, 14, 00, 00)

original_model = tank.run_model(temp_initial,temp_setpoint,t_start,t_finish,increment,0)
# nightcool_model = tank.run_model(temp_initial,temp_setpoint,t_start,t_finish,increment,1)
optimised_model = tank.run_model(temp_initial,temp_setpoint,t_start,t_finish,increment,2)

# Format Data for Table
table_header = ['','Standard','Night Cooling']
table_data = []
# Offpeak
table_data.append(['Off-peak (hours)',original_model['Offpeak Time'],optimised_model['Offpeak Time']])
# Peak
table_data.append(['Peak (hours)',original_model['Peak Time'],optimised_model['Peak Time']])
# Estimated Energy
table_data.append(['Est. Energy (kWh)',round(original_model['Electricity'],2),round(optimised_model['Electricity'],2)])
# Estimated Cooling Time
table_data.append(['Est. Cooling Time (hours)',str(original_model['Total Time']),str(optimised_model['Total Time'])])
# Estimated Energy Cost
table_data.append(['Est. Cost','1 x Elec. Rates', str(round(optimised_model['Cost']/original_model['Cost'],2)) + ' x Rates'])

print (table_data)

# Format data for chart
header = ['t_stamp','nightcool_off','nightcool_on']
data = []
for i, item in enumerate(original_model['Data']):
	data.append([item['Time'].strftime("%Y-%m-%d %H:%M:%S"),item['Temperature'],optimised_model['Data'][i]['Temperature']])

print (header)
for row in data:
	print (row)

# for row in optimized_model:
# 	# print row


# with open('Model with '+str(increment)+' increments.csv', 'wb') as csvfile:
# 	fieldnames = ['Time','Offpeak','Cooling','Cooling Rate','Temperature']
# 	writer = csv.DictWriter(csvfile, fieldnames=fieldnames)	
# 	writer.writeheader()
# 	for row in optimised_model:
# 		newRow = writer.writerow({'Time':row['Time'],'Offpeak': row['Electricity Period'],'Cooling':row['Cooling'],'Cooling Rate':row['Cooling Rate'],'Temperature':row['Temperature']})